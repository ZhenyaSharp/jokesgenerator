﻿namespace JokesGenerator
{
    partial class FormMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.comboBoxNoun = new System.Windows.Forms.ComboBox();
            this.comboBoxVerb = new System.Windows.Forms.ComboBox();
            this.comboBoxNoun2 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // comboBoxNoun
            // 
            this.comboBoxNoun.DropDownHeight = 64;
            this.comboBoxNoun.FormattingEnabled = true;
            this.comboBoxNoun.IntegralHeight = false;
            this.comboBoxNoun.Items.AddRange(new object[] {
            "Дом",
            "Стол",
            "Газета",
            "Огонь",
            "Книга",
            "Туча",
            "Друг",
            "Дождь",
            "Человек",
            "Кровать",
            "Собака",
            "Студент",
            "Письмо",
            "Игра",
            "Земля",
            "Страна",
            "Море",
            "Кофе",
            "Хлеб",
            "Окно",
            "Яблоко",
            "Рука",
            "Нога"});
            this.comboBoxNoun.Location = new System.Drawing.Point(8, 16);
            this.comboBoxNoun.Name = "comboBoxNoun";
            this.comboBoxNoun.Size = new System.Drawing.Size(121, 21);
            this.comboBoxNoun.TabIndex = 0;
            this.comboBoxNoun.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBoxNoun_KeyPress);
            // 
            // comboBoxVerb
            // 
            this.comboBoxVerb.DropDownHeight = 64;
            this.comboBoxVerb.FormattingEnabled = true;
            this.comboBoxVerb.IntegralHeight = false;
            this.comboBoxVerb.Items.AddRange(new object[] {
            "работает",
            "пишет",
            "несёт",
            "гуляет",
            "считает",
            "строит",
            "делает",
            "достигает",
            "отвечает",
            "покупает",
            "опаздывает",
            "стирает",
            "учит",
            "защищает",
            "снимает",
            "разрезает",
            "удаляет",
            "читает",
            "любит",
            "ломает",
            "догоняет",
            "печатает",
            "одевает",
            "готовит"});
            this.comboBoxVerb.Location = new System.Drawing.Point(144, 16);
            this.comboBoxVerb.Name = "comboBoxVerb";
            this.comboBoxVerb.Size = new System.Drawing.Size(121, 21);
            this.comboBoxVerb.TabIndex = 1;
            this.comboBoxVerb.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBoxVerb_KeyPress);
            // 
            // comboBoxNoun2
            // 
            this.comboBoxNoun2.DropDownHeight = 64;
            this.comboBoxNoun2.FormattingEnabled = true;
            this.comboBoxNoun2.IntegralHeight = false;
            this.comboBoxNoun2.Items.AddRange(new object[] {
            "молоко",
            "пингвина",
            "программу",
            "хлеб",
            "оленя",
            "стену",
            "землю",
            "лето",
            "воду",
            "птицу",
            "обои",
            "опилки",
            "каникулы",
            "духи",
            "печенье",
            "шахматы",
            "кашу",
            "суп",
            "очки",
            "макароны",
            "кнопку",
            "собаку",
            "корову",
            "оленя",
            "машину"});
            this.comboBoxNoun2.Location = new System.Drawing.Point(280, 16);
            this.comboBoxNoun2.Name = "comboBoxNoun2";
            this.comboBoxNoun2.Size = new System.Drawing.Size(121, 21);
            this.comboBoxNoun2.TabIndex = 2;
            this.comboBoxNoun2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBoxNoun2_KeyPress);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(144, 128);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(136, 32);
            this.button1.TabIndex = 3;
            this.button1.Text = "Сгенерировать";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox
            // 
            this.textBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox.Location = new System.Drawing.Point(16, 200);
            this.textBox.Multiline = true;
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(384, 32);
            this.textBox.TabIndex = 4;
            this.textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::JokesGenerator.Properties.Resources.clipart34513101;
            this.ClientSize = new System.Drawing.Size(413, 392);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBoxNoun2);
            this.Controls.Add(this.comboBoxVerb);
            this.Controls.Add(this.comboBoxNoun);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Генератор фраз";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxNoun;
        private System.Windows.Forms.ComboBox comboBoxVerb;
        private System.Windows.Forms.ComboBox comboBoxNoun2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox;
    }
}

