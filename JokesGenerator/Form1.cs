﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

namespace JokesGenerator
{
    public partial class FormMain : Form
    {
        private static Random rnd;
        private SoundPlayer soundPlayer;

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            rnd = new Random();
            soundPlayer=new SoundPlayer("madlaugh.wav");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            textBox.Text = comboBoxNoun.Items[rnd.Next(0,comboBoxNoun.Items.Count)]+" "+comboBoxVerb.Items[rnd.Next(0,comboBoxVerb.Items.Count)] +" "+
                           comboBoxNoun2.Items[rnd.Next(0, comboBoxNoun2.Items.Count)];
            soundPlayer.Play();
        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = '\0';
        }

        private void comboBoxNoun_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = '\0';
        }

        private void comboBoxVerb_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = '\0';
        }

        private void comboBoxNoun2_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = '\0';
        }

      
    }
}
